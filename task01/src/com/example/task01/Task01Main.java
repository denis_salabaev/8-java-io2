package com.example.task01;

import java.util.Scanner;

import java.io.File;
import java.io.IOException;

public class Task01Main {
  public static void main(String[] args) throws IOException, InterruptedException {
    // здесь вы можете вручную протестировать ваше решение, вызывая реализуемый
    // метод и смотря результат
    // например вот так:
    System.out.println(extractSoundName(new File("task01/src/resources/3724.mp3")));
  }

  public static String extractSoundName(File file) throws IOException, InterruptedException {
    var processBuilder = new ProcessBuilder("ffprobe", "-v", "error", "-of", "flat", "-show_format", file.getPath());
    Process process = processBuilder.start();

    var scanner = new Scanner(process.getInputStream());
    scanner.useDelimiter("\n");

    while (scanner.hasNext()) {
      String line = scanner.next();

      if (!line.startsWith("format.tags.title")) {
        continue;
      }

      return line.split("=")[1].replaceAll("\"", "");
    }

    return "";
  }
}
